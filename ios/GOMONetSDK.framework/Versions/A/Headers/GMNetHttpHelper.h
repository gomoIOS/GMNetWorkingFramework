//
//  GLApiHelper.h
//  GLive
//
//  Created by Gordon Su on 17/4/7.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMNetHTTPRequest.h"
#import "GMNetHTTPResponse.h"

//FOUNDATION_EXTERN NSString const *RESPONSE;

typedef void (^GMAccountHTTPResponseSucceedHandler)(GMNetHTTPResponse *response);

@interface GMNetHttpHelper : NSObject

- (void)startAsyncWithRequest:(GMNetHTTPRequest *)request finish:(GMAccountHTTPResponseSucceedHandler)finish;

@end
