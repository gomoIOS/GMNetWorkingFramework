//
// Created by zhangjiemin on 2018/6/15.
//

#import <Foundation/Foundation.h>

//extern NSString * kGMDESKey;
//
//extern NSString *kColorCallDESKey;
//
//extern BOOL kGMDebugLog;



@interface GMNetWorkingConfigs : NSObject
+ (instancetype)sharedInstance;

-(NSString *)getColorCallDESKey;
-(NSString *) getDesKy;
-(BOOL)getDebugLog;

- (void) setDebugLog:(BOOL) debugLog;

- (void)setEnvtType:(BOOL)isTest;

- (void)setDesKey:(NSString *) desKey;
- (void)setColorCallDESKey:(NSString *)desKey;

- (NSString *)getEnvtType;

- (void)setDesBytesCapacity:(long long)capacity;

- (long long)getDesBytesCapacity;
@end
