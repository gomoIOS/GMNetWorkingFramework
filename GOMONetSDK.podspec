Pod::Spec.new do |s|
  s.name = "GOMONetSDK"
  s.version = "0.2.0.6"
  s.summary = "A short description of GOMONetSDK."
  s.license = {"type"=>"MIT", "file"=>"LICENSE"}
  s.authors = {"zhangjiemin000"=>"zhangjiemin000@gmail.com"}
  s.homepage = "https://gitlab.com/gomoIOS/GOMONetSDK"
  s.description = "TODO: Add long description of the pod here."
  s.libraries = "resolv"
  s.source = { :path => '.' }

  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'ios/GOMONetSDK.framework'
end
